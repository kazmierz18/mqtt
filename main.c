#include "helpers.h"
#include <MQTTClient.h>
#include <stdbool.h>
#include <string.h>

volatile MQTTClient_deliveryToken delivered_token;
volatile bool received = false;

/**
 * Callback called when message was delivered
 */
void delivered(void *context, MQTTClient_deliveryToken dt) {
  INFO("Message with token value %d delivery confirmed\n", dt);
  delivered_token = dt;
}

/**
 * Callback for subscribed messages
 */
int message_arrived(void *context, char *topicName, int topicLen,
                     MQTTClient_message *message) {
  int i;
  char *payload_ptr;
  INFO("Message arrived topic: '%s' msg: '", topicName);
  payload_ptr = message->payload;
  for (i = 0; i < message->payloadlen; i++) {
    putchar(*payload_ptr++);
  }
  puts("'\n");
  MQTTClient_freeMessage(&message);
  MQTTClient_free(topicName);
  received = true;
  return 1;
}

/**
 * Callback for connection disruption
 */
void connection_lost(void *context, char *cause) {
  ERROR("\nConnection lost\n");
  ERROR("     cause: %s\n", cause);
}

int main() {
  MQTTClient handle;
  const char *serverURI = "tcp://localhost:1883";
  const char *clientId = "clientId";
  const int persistence_type = 1;
  void *persistence_context = NULL;

  INFO("Connecting to mqtt broker\n");
  int res = MQTTClient_create(&handle, serverURI, clientId, persistence_type,
                              persistence_context);
  if (res != MQTTCLIENT_SUCCESS) {
    ERROR("Could not create mqtt client: %d\n", res);
    PANIC();
  }
  res = MQTTClient_setCallbacks(handle, NULL, connection_lost, message_arrived,
                                delivered);
  if (res != MQTTCLIENT_SUCCESS) {
    ERROR("Could not set callbacks: %d\n", res);
    PANIC();
  }
  const char *topicName = "rooms/room1/sensor/temp";

  MQTTClient_connectOptions options = MQTTClient_connectOptions_initializer;

  // Last will setup
  MQTTClient_willOptions last_will = MQTTClient_willOptions_initializer;
  last_will.payload.len = strlen("BYE");
  last_will.payload.data = "BYE";
  last_will.topicName = topicName;
  options.will = &last_will;

  // Connection setup
  options.keepAliveInterval = 20;
  options.cleansession = 1;

  res = MQTTClient_connect(handle, &options);
  if (res != MQTTCLIENT_SUCCESS) {
    ERROR("Could not connect to mqtt broker: %d\n", res);
    PANIC();
  }

  // =============================================
  // Send message test
  MQTTClient_message msg = MQTTClient_message_initializer;
  msg.payload = "123";
  msg.payloadlen = strlen(msg.payload);
  msg.qos = 1;
  MQTTClient_deliveryToken dt = 0;
  res = MQTTClient_publishMessage(handle, topicName, &msg, &dt);
  if (res != MQTTCLIENT_SUCCESS) {
    ERROR("Could not publish message: %d\n", res);
    PANIC();
  }
  while (delivered_token != dt)
    ;

  // =============================================
  // Subscription test
  MQTTClient_subscribe(handle, topicName, 1);
  if (res != MQTTCLIENT_SUCCESS) {
    ERROR("Could not subscribe: %d\n", res);
    PANIC();
  }
  while (!received)
    ;

  // Cleanup
  INFO("Closing connection...\n");
  const int timeout = 10;
  res = MQTTClient_disconnect(handle, timeout);
  if (res != MQTTCLIENT_SUCCESS) {
    ERROR("Could not disconnect to mqtt broker: %d\n", res);
    PANIC();
  }
  MQTTClient_destroy(&handle);
}
